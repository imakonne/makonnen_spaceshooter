﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject playerExplosion;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary")
        {
            return;
        }
        Instantiate(explosion, transform.position, transform.rotation);
        if (other.tag == "Player")
        {
           Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
           //GameController.GameOver();
        }

       // if(other.tag == "Pickup")
           if(other.gameObject.CompareTag("Pickup"))
        {
            Destroy(other.gameObject);
        }
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
}
