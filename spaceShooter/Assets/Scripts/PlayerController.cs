﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}
public class PlayerController : MonoBehaviour
{
    public float speed;
    public float tilt;
    public Boundary boundary;
   

    public GameObject[] shots; 
    public Transform shotSpawn;
    public float fireRate;

    private float nextFire;

    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            GameObject shot = shots[(int)(Random.value * shots.Length)];
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Pickup")
        {
            print("Speed boosted!");
            speed += 5f;
            Destroy(other.gameObject, 0);
        }
                
            

        if (other.tag == "size")
        {

            print("Size boosted!");
            transform.localScale += new Vector3(.5f, .5f, .5f);
            Destroy(other.gameObject, 0);
        }
            
   
    }
    //Script modification 
    //ONE OF MY SCRIPT MODIFICATIONS 1/2
    //this piece of code is for the ship, after it picks up an object.. This script makes the ship bigger after every time it gets a pick up.
    //player should avoiud this sphere pickup
    


    //Script modificaion 2/2
    //change of speed, so everytime hip picks up the obect, speed increases
  
        
   
       //this specific line of code is the if statement for when the pickup has th pickup tag   // if(other.tag == "Pickup")
                                                                            //if(other.gameObject.CompareTag("Pickup"))


        //3rd modification, i changed the bolt color to blue, but i wanted to do mor and randomize te bolt colors after every time it shoots, from orange back to blue. or blue to orange, etc. no repeating paterns.

        //spare mod:
        //wanted to add a pickup that added multiple sht spawn and shots, so th ship would be more powerful in terms of destroying the asteroids.


void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis ("Horizontal");
        float moveVertical = Input.GetAxis ("Vertical");

        Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
        GetComponent<Rigidbody>().velocity = movement * speed;
        
        GetComponent<Rigidbody>().position = new Vector3
        (
            Mathf.Clamp (GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp (GetComponent<Rigidbody>().position.z, boundary.zMin, boundary.zMax)

        );
        
        GetComponent<Rigidbody>().rotation = Quaternion.Euler (0.0f, 0.0f, GetComponent<Rigidbody>().velocity.x * -tilt);
        
    }
}
    

